package pl.edu.agh.mobilne.gdp.algorithm;

import pl.agh.mobilne.mqtt.exception.DeviceCommunicationException;
import pl.agh.mobilne.mqtt.exception.DeviceNotFoundException;
import pl.agh.mobilne.mqtt.model.messages.*;
import pl.agh.mobilne.mqtt.model.*;

public class GatewayNode {
	
	private Device device;
	private String id;
	private byte radius = 0;
	private int period = 3000;
	
	private boolean sendAdverts = true;
	private boolean connected = true;
	private boolean debugMode = true;
	

	public GatewayNode(Device device, String id) {
		this.device = device;
		this.id = id;
		
		
		device.addMessageListener((from, message) -> {
        	if("SEARCHGW".equals(message.getMessageHeader().getType().toString())) {
        		
        		if(this.debugMode) {
	        		System.out.println(message.getBody().toString());
	        		System.out.println("*********** SearchGW received ***********");
	        		System.out.println(from.getId());
        		}
        		
        		try {
        			device.sendMessage(new Address(from.getId()), new GwinfoMessage((byte)0, new Address(this.id).getId().getBytes()));
	    			
        			if(this.debugMode) {
        				System.out.println("*********** GwInfo sent ***********");
        			}
	    		} catch (DeviceCommunicationException e1) {
	    			e1.printStackTrace();
	    		} catch (DeviceNotFoundException e1) {
	    			e1.printStackTrace();
	    		}

        	}
        
		});
		
		
		
		device.addMessageListener((from, message) -> {
        	if("PINGREQ".equals(message.getMessageHeader().getType().toString())) {
        		
        		if(this.debugMode) {
	        		System.out.println(message.getBody().toString());
	        		System.out.println("*********** PingReq received ***********");
	        		System.out.println(from.getId());
        		}
        		
        		if(this.connected) {
	        		try {
	        			device.sendMessage(new Address(from.getId()), new PingrespMessage());
		    			
	        			if(this.debugMode) {
	        				System.out.println("*********** PingResp sent ***********");
	        			}
		    		} catch (DeviceCommunicationException e1) {
		    			e1.printStackTrace();
		    		} catch (DeviceNotFoundException e1) {
		    			e1.printStackTrace();
		    		}
        		}

        	}
        
		});
		
	}
	
	
	
	public GatewayNode(Device device, String id, byte radius, int period) {
		this.device = device;
		this.id = id;
		this.radius = radius;
		this.period = period;
		
		device.addMessageListener((from, message) -> {
        	if("SEARCHGW".equals(message.getMessageHeader().getType().toString())) {
        		
        		if(this.debugMode) {
	        		System.out.println(message.getBody().toString());
	        		System.out.println("*********** SearchGW received ***********");
	        		System.out.println(from.getId());
        		}
        		
        		try {
        			device.sendMessage(new Address(from.getId()), new GwinfoMessage((byte)0, new Address(this.id).getId().getBytes()));
	    			
        			if(this.debugMode) {
        				System.out.println("*********** GwInfo sent ***********");
        			}
	    		} catch (DeviceCommunicationException e1) {
	    			e1.printStackTrace();
	    		} catch (DeviceNotFoundException e1) {
	    			e1.printStackTrace();
	    		}

        	}
        
		});
		
		
		
		device.addMessageListener((from, message) -> {
        	if("PINGREQ".equals(message.getMessageHeader().getType().toString())) {
        		
        		if(this.debugMode) {
	        		System.out.println(message.getBody().toString());
	        		System.out.println("*********** PingReq received ***********");
	        		System.out.println(from.getId());
        		}
        		
        		if(this.connected) {
	        		try {
	        			device.sendMessage(new Address(from.getId()), new PingrespMessage());
		    			
	        			if(this.debugMode) {
	        				System.out.println("*********** PingResp sent ***********");
	        			}
		    		} catch (DeviceCommunicationException e1) {
		    			e1.printStackTrace();
		    		} catch (DeviceNotFoundException e1) {
		    			e1.printStackTrace();
		    		}
        		}
        	}
        
		});
		
	}



	public void broadcastAdverts() {
		
		while (sendAdverts) {
			try {
				this.device.sendMessage(new Address(Address.BROADCAST.getId()), new AdvertiseMessage((byte)0, this.radius));
				Thread.sleep(this.period);
			} catch (DeviceCommunicationException e) {
				e.printStackTrace();
			} catch (DeviceNotFoundException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void broadcastAdvertOnce() {
	
		if (sendAdverts) {
			try {
				this.device.sendMessage(new Address(Address.BROADCAST.getId()), new AdvertiseMessage((byte)0, this.radius));
			} catch (DeviceCommunicationException e) {
				e.printStackTrace();
			} catch (DeviceNotFoundException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	public void runProtocol() {
		
		broadcastAdverts();
		
	}
	
	
	public boolean isSendAdverts() {
		return sendAdverts;
	}



	public void setSendAdverts(boolean sendAdverts) {
		this.sendAdverts = sendAdverts;
	}



	public byte getRadius() {
		return radius;
	}



	public void setRadius(byte radius) {
		this.radius = radius;
	}

	
	public int getPeriod() {
		return period;
	}



	public void setPeriod(int period) {
		this.period = period;
	}


	public boolean isConnected() {
		return connected;
	}



	public void setConnected(boolean connected) {
		this.connected = connected;
	}



	public boolean isDebugMode() {
		return debugMode;
	}



	public void setDebugMode(boolean debugMode) {
		this.debugMode = debugMode;
	}


}

