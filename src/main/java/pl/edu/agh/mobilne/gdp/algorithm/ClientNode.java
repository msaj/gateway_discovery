package pl.edu.agh.mobilne.gdp.algorithm;

import java.util.TreeSet;

import pl.agh.mobilne.mqtt.exception.DeviceCommunicationException;
import pl.agh.mobilne.mqtt.exception.DeviceNotFoundException;
import pl.agh.mobilne.mqtt.model.messages.*;
import pl.agh.mobilne.mqtt.model.*;


public class ClientNode {
	
	private static final int GW_NUMBER = 3;
	
	private int listSize;
	private TreeSet<Address> gwList;
	
	private Device device;
	private String id;
	private byte radius = 0;
	private int pingPeriod = 1000;
	
	private boolean found = false;
	private boolean pingResp = true;
	
	private boolean pingGateway = true;
	
	private boolean debugMode = true;
	
	public ClientNode(Device device, String id) {
		this.listSize = GW_NUMBER;
		gwList = new TreeSet<Address>(new GwComparator());
		this.device = device;
		this.id = id;
		
		
		device.addMessageListener((from, message) -> {
        	
        	if("GWINFO".equals(message.getMessageHeader().getType().toString())) {
        		
        		if(this.debugMode) {
	        		System.out.println("*********** GwInfo received ***********");
	        		System.out.println(message.getBody().toString());
	        		System.out.println(from.getId());
        		}
        		
        		addGw(from);
	        	found = true;
	        	
	        	if(this.debugMode) {
	        		System.out.println("GWList.size() = " + this.gwList.size());
	        	}
        	}
        
        });
		
		device.addMessageListener((from, message) -> {
        	
        	if("PINGRESP".equals(message.getMessageHeader().getType().toString())) {
        		
        		if(this.debugMode) {
	        		System.out.println("*********** PingResp received ***********");
	        		System.out.println(message.getBody().toString());
	        		System.out.println(from.getId());
        		}
        		
        		this.pingResp = true;
        	}
        
        });
		
		
		device.addMessageListener((from, message) -> {
        	
        	if("ADVERTISE".equals(message.getMessageHeader().getType().toString())) {
        		
        		if(this.debugMode) {
	        		System.out.println("*********** Advertise received ***********");
	        		System.out.println(message.getBody().toString());
	        		System.out.println(from.getId());
        		}
        		
        		addGw(from);
	        	found = true;
	        	
	        	if(this.debugMode) {
	        		System.out.println("GWList.size() = " + this.gwList.size());
	        	}
        	}
        
        });

	}
		
	public ClientNode(Device device, String id, byte radius, int listSize, int pingPeriod) {
		this.listSize = listSize;
		gwList = new TreeSet<Address>(new GwComparator());
		this.device = device;
		this.id = id;
		this.radius = radius;
		this.pingPeriod = pingPeriod;
		
		device.addMessageListener((from, message) -> {
        	
        	if("GWINFO".equals(message.getMessageHeader().getType().toString())) {
        		
        		if(this.debugMode) {
	        		System.out.println("*********** GwInfo received ***********");
	        		System.out.println(message.getBody().toString());
	        		System.out.println(from.getId());
        		}
        		
        		addGw(from);
	        	found = true;
	        	
	        	if(this.debugMode) {
	        		System.out.println("GWList.size() = " + this.gwList.size());
	        	}
        	}
        
        });
		
		device.addMessageListener((from, message) -> {
        	
        	if("PINGRESP".equals(message.getMessageHeader().getType().toString())) {
        		
        		if(this.debugMode) {
	        		System.out.println("*********** PingResp received ***********");
	        		System.out.println(message.getBody().toString());
	        		System.out.println(from.getId());
        		}
        		
        		this.pingResp = true;
        	}
        
        });
		
		
		device.addMessageListener((from, message) -> {
        	
        	if("ADVERTISE".equals(message.getMessageHeader().getType().toString())) {
        		
        		if(this.debugMode) {
	        		System.out.println("*********** Advertise received ***********");
	        		System.out.println(message.getBody().toString());
	        		System.out.println(from.getId());
        		}
        		
        		addGw(from);
	        	found = true;
	        	
	        	if(this.debugMode) {
	        		System.out.println("GWList.size() = " + this.gwList.size());
	        	}
        	}
        
        });
        	
	}
		


	public void searchGateway() {
		
		if (!this.found) {
			try {
				device.sendMessage(new Address(Address.BROADCAST.getId()), new SearchgwMessage((byte)0));
				if(this.debugMode) {
					System.out.println("*********** SearchGW sent ***********");
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			} catch (DeviceCommunicationException e1) {
				e1.printStackTrace();
			} catch (DeviceNotFoundException e1) {
				e1.printStackTrace();
			}
			
		if(this.gwList.size() > 0)
			this.found = true;
			
		}
		
	}
	
	public void ping() {
		
		if (this.pingResp && this.gwList.first() != null) {
			try {
				this.device.sendMessage(this.gwList.first(), new PingreqMessage(this.id.getBytes()));
				
				if(this.debugMode) {
					System.out.println("*********** PingReq sent ***********");
				}
				
				this.pingResp = false;
				
			} catch (DeviceCommunicationException e) {
				e.printStackTrace();
			} catch (DeviceNotFoundException e) {
				e.printStackTrace();
			}
			
		}
		else if (!this.pingResp && this.gwList.first() != null) {
			gwList.remove(gwList.first());
			this.pingResp = true;

		}
		else if (this.gwList.first() == null) {
			searchGateway();
		}
		
	}
	
	
	public void pingGateway() {
		
		while(this.pingGateway) {
			
			ping();
			try {
				Thread.sleep(this.pingPeriod);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	public void addGw(Address gwId) {
		
		if(!gwList.contains(gwId) && gwList.size() < listSize)
			gwList.add(gwId);
		
	}
	
	
	public Address getGW() {
		
		if(this.gwList.size() > 0)
			return this.gwList.first();
		else {
			searchGateway();
			
			if(this.gwList.size() > 0)
				return this.gwList.first();
			else return Address.BROADCAST;
		}
	}
	
	
	public void runProtocol() {
		
		searchGateway();
		pingGateway();
		
	}

	

	public int getListSize() {
		return listSize;
	}

	public void setListSize(int listSize) {
		this.listSize = listSize;
	}

	public byte getRadius() {
		return radius;
	}

	public void setRadius(byte radius) {
		this.radius = radius;
	}

	public boolean isDebugMode() {
		return debugMode;
	}

	public void setDebugMode(boolean debugMode) {
		this.debugMode = debugMode;
	}

	public static int getGwNumber() {
		return GW_NUMBER;
	}

	public boolean isPingGateway() {
		return pingGateway;
	}

	public void setPingGateway(boolean pingGateway) {
		this.pingGateway = pingGateway;
	}

	public int getPingPeriod() {
		return pingPeriod;
	}

	public void setPingPeriod(int pingPeriod) {
		this.pingPeriod = pingPeriod;
	}

}


